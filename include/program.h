#ifndef PROGRAM_H
#define PROGRAM_H

#include <stdio.h>
#include <stdlib.h>

#include "functions.h"

// returns an error code 
GLuint load_and_compile_shader(GLuint* shader, GLint type, char const* filename);

GLuint link_program(GLuint program,
    GLuint vertex_shader, 
    GLuint tess_ctrl_shader, 
    GLuint tess_eval_shader, 
    GLuint geometry_shader, 
    GLuint fragment_shader, 
    GLuint compute_shader);

void build_program(GLuint* program);

#endif // !PROGRAM_H
