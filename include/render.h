#ifndef RENDER_H
#define RENDER_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "functions.h"
#include "program.h"

typedef struct 
{
    GLuint program;
    GLuint x;
    GLuint y;
    GLuint width;
    GLuint height;
}RenderState;

RenderState render_state;

void init_render_state(void);
void init(void);
void render(void);

#endif // !RENDER_H
