#ifdef __linux__

#include <GL/glx.h>
#include <GL/gl.h>

#include "functions.h"
#include "render.h"
 
#define GLX_CONTEXT_MAJOR_VERSION_ARB		0x2091
#define GLX_CONTEXT_MINOR_VERSION_ARB		0x2092
typedef GLXContext (*GLXCREATECONTEXTATTRIBSARBPROC)(Display*, GLXFBConfig, GLXContext, Bool, const int*);

int main (int argc, char ** argv)
{
	Display *dpy = XOpenDisplay(0);
 
	int nelements;
	GLXFBConfig *fbc = glXChooseFBConfig(dpy, DefaultScreen(dpy), 0, &nelements);
 
	static int attributeList[] = { GLX_RGBA, GLX_DOUBLEBUFFER, GLX_RED_SIZE, 1, GLX_GREEN_SIZE, 1, GLX_BLUE_SIZE, 1, None };
	XVisualInfo *vi = glXChooseVisual(dpy, DefaultScreen(dpy),attributeList);
 
	XSetWindowAttributes swa;
	swa.colormap = XCreateColormap(dpy, RootWindow(dpy, vi->screen), vi->visual, AllocNone);
	swa.border_pixel = 0;
	swa.event_mask = StructureNotifyMask;

    init_render_state();

	Window win = XCreateWindow(dpy, 
        RootWindow(dpy, vi->screen), 
        render_state.x, 
        render_state.y, 
        render_state.width, 
        render_state.height, 
        0, 
        vi->depth, 
        InputOutput, 
        vi->visual, 
        CWBorderPixel|CWColormap|CWEventMask, 
        &swa);
 
	XMapWindow (dpy, win);
 
	GLXCREATECONTEXTATTRIBSARBPROC glXCreateContextAttribsARB = (GLXCREATECONTEXTATTRIBSARBPROC) glXGetProcAddress((const GLubyte*)"glXCreateContextAttribsARB");
 
	int attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB, 3,
		GLX_CONTEXT_MINOR_VERSION_ARB, 2,
		0};
 
	GLXContext ctx = glXCreateContextAttribsARB(dpy, *fbc, 0, 1, attribs);
 
    glXMakeCurrent (dpy, win, ctx);
    
    init_functions();

    init();

    while (1)
    {
        render();
	    glXSwapBuffers (dpy, win);
    }
 
	ctx = glXGetCurrentContext(); 
	glXDestroyContext(dpy, ctx); 
}

#endif //__linux__
