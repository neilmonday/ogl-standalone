#include "render.h"

void init_render_state(void)
{
    render_state.program = 0;
    render_state.x = 100;
    render_state.y = 100;
    render_state.width = 320;
    render_state.height = 180;
}

void init(void)
{
    GLint major;
    GLint minor;
    glGetIntegerv(GL_MAJOR_VERSION, &major);
    glGetIntegerv(GL_MINOR_VERSION, &minor);

    // Initialize 
    build_program(&render_state.program);

    //create a VAO
    GLuint vertex_array_object = 0;
    glGenVertexArrays(1, &vertex_array_object);
    glBindVertexArray(vertex_array_object);

    GLfloat vertices[] = {
        -1.0f, -1.0f,
        1.0f, -1.0f,
        -1.0f,  1.0f,
        1.0f, -1.0f,
        1.0f,  1.0f,
        -1.0f,  1.0f
    };

    //create a VBO
    GLuint vertex_buffer_object;
    glGenBuffers(1, &vertex_buffer_object);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object);
    glBufferData(GL_ARRAY_BUFFER, 6 * 2 * 4, vertices, GL_STATIC_DRAW);

    GLint position = glGetAttribLocation(render_state.program, "position");
    glVertexAttribPointer(position, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(position);

    return;
}

void render(void)
{
    // Clear
    const GLfloat color[] = { 0.5f, 0.5f, 0.0f, 1.0f };
    glClearBufferfv(GL_COLOR, 0, color);

    // Draw the image to the screen
    glUseProgram(render_state.program);
    glDrawArrays(GL_TRIANGLES, 0, 6);
}
