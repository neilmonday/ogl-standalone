#include "program.h"

char const* vertex_shader_source = 
"#version 450 core\n"
"in vec2 position;\n"
"void main()\n"
"{\n"
"	gl_Position = vec4(position, 0.0f, 1.0f);\n"
"}\n\0";

char const* fragment_shader_source =
"#version 450 core\n"
"out vec4 color;\n"
"void main(){\n"
"    color = vec4(1., 0., 1., 1.);\n"
"}\n";

// returns an error code 
GLuint load_and_compile_shader(GLuint* shader, GLint type, char const* source)
{

    GLint isCompiled = 0;
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &isCompiled);
    if (isCompiled == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &maxLength);

        //The maxLength includes the NULL character
        GLchar* infoLog = (GLchar*)malloc(maxLength * sizeof(GLchar));
        glGetShaderInfoLog(*shader, maxLength, &maxLength, &infoLog[0]);

        //We don't need the shader anymore.
        glDeleteShader(*shader);

        //Use the infoLog as you see fit.
        printf("%s", infoLog);

        //In this simple program, we'll just leave
        return 1;
    }
    return 0;
}

GLuint link_program(GLuint program, GLuint vertex_shader, GLuint tess_ctrl_shader, GLuint tess_eval_shader, GLuint geometry_shader, GLuint fragment_shader, GLuint compute_shader)
{
    // Create program, attach shaders to it, and link it 
    if (vertex_shader) glAttachShader(program, vertex_shader);
    if (tess_ctrl_shader) glAttachShader(program, tess_ctrl_shader);
    if (tess_eval_shader) glAttachShader(program, tess_eval_shader);
    if (geometry_shader) glAttachShader(program, geometry_shader);
    if (fragment_shader) glAttachShader(program, fragment_shader);
    if (compute_shader) glAttachShader(program, compute_shader);
    glLinkProgram(program);
    GLint isLinked = 0;
    glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
    if (isLinked == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

        //The maxLength includes the NULL character
        GLchar* infoLog = (GLchar*)malloc(maxLength * sizeof(GLchar));
        glGetProgramInfoLog(program, maxLength, &maxLength, &infoLog[0]);

        //The program is useless now. So delete it.
        glDeleteProgram(program);

        printf("%s", infoLog);

        //Exit with failure.
        return 1;
    }
    return 0;
}

void build_program(GLuint* program)
{
    if (*program != 0)
    {
        glUseProgram(0);
        glDeleteProgram(*program);
        *program = 0;
    }
    GLuint vertex_shader = 0, fragment_shader = 0;
    load_and_compile_shader(&vertex_shader, GL_VERTEX_SHADER, vertex_shader_source);
    load_and_compile_shader(&fragment_shader, GL_FRAGMENT_SHADER, fragment_shader_source);
    *program = glCreateProgram();
    link_program(*program, vertex_shader, 0, 0, 0, fragment_shader, 0);
    if (vertex_shader) glDeleteShader(vertex_shader);
    if (fragment_shader) glDeleteShader(fragment_shader);

    glUseProgram(*program);
    GLint texture_location = glGetUniformLocation(*program, "texture");
    glUniform1i(texture_location, 0);
}
