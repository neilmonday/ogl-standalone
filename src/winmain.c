#ifdef _WIN32
#include <Windows.h>
#include <GL/gl.h>
#include "GL/wglext.h"

#include "functions.h"
#include "render.h"

HGLRC hGLRC;
HDC hDC;
typedef DWORD64 DHGLRC;
#define FAST_CONTEXT_CREATION

LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
    switch (msg) {
        case WM_CREATE:
        {
            PIXELFORMATDESCRIPTOR pfd;
            memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));
            pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
            pfd.nVersion = 1;
            pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
            pfd.iPixelType = PFD_TYPE_RGBA;
            pfd.cColorBits = 32;
            pfd.cDepthBits = 32;
            pfd.iLayerType = PFD_MAIN_PLANE;

            hDC = GetWindowDC(hWnd);
            //DrvCreateContextPtr(hDC);
            int nPixelFormat = ChoosePixelFormat(hDC, &pfd);

            if (nPixelFormat == 0) return -1;
            BOOL bResult = SetPixelFormat(hDC, nPixelFormat, &pfd);

            if (!bResult) return -1;

#ifndef FAST_CONTEXT_CREATION
            int major = 4, minor = 5;
            HGLRC tempContext = wglCreateContext(hDC);
            wglMakeCurrent(hDC, tempContext);
            int attribs[] =
            {
                WGL_CONTEXT_MAJOR_VERSION_ARB, major,
                WGL_CONTEXT_MINOR_VERSION_ARB, minor,
                WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,// | WGL_CONTEXT_DEBUG_BIT_ARB,
                WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB, //0x9126 , 0x00000001,
                0
            };

            PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB = NULL;
            wglCreateContextAttribsARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC)wglGetProcAddress("wglCreateContextAttribsARB");
            if (wglCreateContextAttribsARB != NULL)
            {
                hGLRC = wglCreateContextAttribsARB(hDC, 0, attribs);
            }

            wglMakeCurrent(hDC, hGLRC);
#else // FAST_CONTEXT_CREATION
            hGLRC = wglCreateContext(hDC);
            wglMakeCurrent(hDC, hGLRC);
#endif // FAST_CONTEXT_CREATION
            glGetString(GL_VERSION);
            init_functions();

            init();

            //wglMakeCurrent(NULL, NULL);
            //wglDeleteContext(tempContext);
            return 0;
        }
        case WM_PAINT:
        {
            wglMakeCurrent(hDC, hGLRC);
            render();
            wglSwapLayerBuffers(hDC, WGL_SWAP_MAIN_PLANE);
            wglMakeCurrent(NULL, NULL);
            return 0;
        }
        case WM_DESTROY:
        {
            PostQuitMessage(0);
            return 0;
        }
    }

    return DefWindowProc(hWnd, msg, wParam, lParam);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR pCmdLine, int nCmdShow)
{
    //Window class name
    const char windowName[] = "OGL-STANDALONE";

    //Set up window class
    WNDCLASS wnd = { 0 };
    wnd.lpfnWndProc = WndProc;
    wnd.hInstance = NULL;
    wnd.lpszClassName = windowName;
    wnd.style = CS_OWNDC;

    //Register window class
    RegisterClass(&wnd);
    
    init_render_state();

    HWND hwnd = CreateWindowEx(WS_EX_APPWINDOW,
        "OGL-STANDALONE",
        windowName,
        WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
        render_state.x, render_state.y,
        render_state.width, render_state.height,
        NULL,
        NULL,
        NULL,
        NULL);

    //Simple check to see if window creation failed
    if (hwnd == NULL)
    {
        return 0;
    }

    ShowWindow(hwnd, nCmdShow);

    // Run the message loop.
    MSG msg = { 0 };
    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return TRUE;
}

#endif //_WIN32
